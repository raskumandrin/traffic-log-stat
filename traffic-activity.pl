#!/usr/bin/perl
use strict;
use warnings;

my $log_pattern = q{([\S]+) - - \[[^:]+:(\d+)[^\]]+\] "([\S]+) ([\S]+) ([^"]+)" (\d+) (\d+) "([^"]+)" "([^"]+)"};

my %hour;
my %act;

while (<>) {
	chomp;
	/$log_pattern/;
	
	if ( $2 and $7 ) {
		$hour{$2} += $7;
		$act{$2} ++;
	}
}

print "hour\ttraffic\tactivities\n";
print "$_\t$hour{$_}\t$act{$_}\n" foreach sort keys %hour;