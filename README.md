# Task

We would like to analyze our data traffic. Our best approach we currently can go for, is to analyze our access_logs. These are plain text files with all GET and POST activities, for example like below:

```
62.140.132.89 - - [25/Feb/2014:06:39:51 +0100] "GET /assets/default/javascripts/jquery.simplyscroll.js HTTP/1.1" 200 3408 "-" "Mozilla/5.0 (Linux; Android 4.3; GT-I9305 Build/JSS15J) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.99 Mobile Safari/537.36"
```

Where 3408 is the amount of bytes. 

Every day, these files are stored PER domain, automatically. as .GZ files. When unzipped, they're rather big of course as some of our sites have over 130K visitors a day.

What we would like for you to be done, is create a PHP script that simply does the following:

- Loop through the files per domain (every day)
- (1-1) Calculate total traffic (so total (mega)bytes) per day and hour
- (2) Calculate the top 100 files that generated the most traffic
- (3) Calculate the top 100 IP addresses that generated the most traffic
- (1-2) calculate # amount of activities (so the total of lines in these log files) per day and hour
- Store this in a CSV file

Please note, again, these files can become rather big when unzipped (we're talking GB's) so please remember there is a limited amount of RAM to use (meaning: You can't just "include()").

As for other statusses... For now, I only would need 200 http and 404 is indeed very interesting too.
(4) However, with 404 I would only need the files, number of times it happend and referers.

We only need 1.gz for each domain. The other ones are just backup for previous days.
So to keep this short: we only need to process www.DOMAIN.TLD.access.log.1.gz

# Script

there are 5 files

gz2csv.bash - main script with settings

not-found.pl, top-files.pl, top-ips.pl, traffic-act.pl (counters; don't need any modifications)

all files must be in same directory

you also may run gz2csv.bash from cron

before running - set 3 parameters inside:
LOG_DIR - dir, where located logs
CSV_DIR - dir to put result csv
TMP_FILE - name of temp file (don't need modification)

dirs paths should be absolute

this script will loop through files by mask '*access.log.1.gz' and count result


before running, check that all five files have permission for executing.

also i have added top 1000 limit (it can be easily changed in future)


to run - unzip, write paths and run
«./gz2csv.bash» from current dir
or
«/full-path/gz2csv.bash» (better for cron)

# Input data update

However, we do have a problem. I discussed with the hoster and it turns out that in the end there are multiple files per domain. Don't ask me why, but this is the case. Up to three files per domain for which we have these logs.

The files are exactly the same, just have different access_log data.
So shall we say we name the files
- www.domain.tld.access.log.1.gz
- www.domain.tld.access.log.2.gz
- www.domain.tld.access.log.3.gz

And I will have them all in one directory. So you can sum them and analyse them.
