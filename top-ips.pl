#!/usr/bin/perl
use strict;
use warnings;

my $log_pattern = q{([\S]+) - - \[[^:]+:(\d+)[^\]]+\] "([\S]+) ([\S]+) ([^"]+)" (\d+) (\d+) "([^"]+)" "([^"]+)"};

my %ip;

while (<>) {
	chomp;
	/$log_pattern/;
	
	if ( $1 and $7 ) {
		$ip{$1} += $7;
	}
}

print "traffic\tip\n";
print "$ip{$_}\t$_\n" foreach reverse sort { $ip{$a} <=> $ip{$b} } keys %ip;
