#!/usr/bin/perl
use strict;
use warnings;

my $log_pattern = q{([\S]+) - - \[[^:]+:(\d+)[^\]]+\] "([\S]+) ([\S]+) ([^"]+)" (\d+) (\d+) "([^"]+)" "([^"]+)"};

my $nofile;

while (<>) {
	chomp;
	/$log_pattern/;
	
	if ( $4 and $6 == 404 ) {
		$nofile->{$4}{count}++;
		$nofile->{$4}{ref}{$8}++
	}
}

#foreach my $url (reverse sort { $nofile->{$a}{count} <=> $nofile->{$b}{count} } keys %$nofile) {
#	print "$nofile->{$url}{count} : $url\n" ;
#	foreach (reverse sort { $nofile->{$url}{ref}{$a} <=> $nofile->{$url}{ref}{$b} } keys %{$nofile->{$url}{ref}}) {
#		print "\t$nofile->{$url}{ref}{$_} : $_\n" ;
#	}
#}

print "error count\tdocument\treferrer\n";

foreach my $url (reverse sort { $nofile->{$a}{count} <=> $nofile->{$b}{count} } keys %$nofile) {
	foreach (reverse sort { $nofile->{$url}{ref}{$a} <=> $nofile->{$url}{ref}{$b} } keys %{$nofile->{$url}{ref}}) {
		print "$nofile->{$url}{ref}{$_}\t$url\t$_\n" ;
	}
}