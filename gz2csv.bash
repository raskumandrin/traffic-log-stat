#!/bin/bash

# source dir with gz-log files
LOG_DIR=/home/stas/fuzzy-shame/source

# output dir for csv-files
CSV_DIR=/home/stas/fuzzy-shame/data

# gz-log will be extracted to this temp file
# after this will be processed all count script on this file
# finally file will be deleted
TMP_FILE=/tmp/domain.log

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

mkdir -p $CSV_DIR

APP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CUR_DATE=$(date +%Y-%m-%d)

for i in $( find $LOG_DIR -maxdepth 1 -type f -name '*access.log.1.gz' ); do

DOMAIN=$(echo $i | sed 's/[.]access[.]log[.]1[.]gz//' | sed 's/.*\///' )

gunzip -c $LOG_DIR/$DOMAIN.access.log.1.gz > $TMP_FILE
gunzip -c $LOG_DIR/$DOMAIN.access.log.2.gz >> $TMP_FILE
gunzip -c $LOG_DIR/$DOMAIN.access.log.3.gz >> $TMP_FILE

$APP_DIR/traffic-activity.pl $TMP_FILE > "$CSV_DIR/$DOMAIN-$CUR_DATE-traffic-activity.csv"
$APP_DIR/top-ips.pl $TMP_FILE | head -1001 > "$CSV_DIR/$DOMAIN-$CUR_DATE-top-ips.csv"
$APP_DIR/top-files.pl $TMP_FILE | head -1001 > "$CSV_DIR/$DOMAIN-$CUR_DATE-top-files.csv"
$APP_DIR/not-found.pl $TMP_FILE > "$CSV_DIR/$DOMAIN-$CUR_DATE-not-found.txt"

rm $TMP_FILE

done