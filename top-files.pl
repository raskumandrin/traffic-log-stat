#!/usr/bin/perl
use strict;
use warnings;

my $log_pattern = q{([\S]+) - - \[[^:]+:(\d+)[^\]]+\] "([\S]+) ([\S]+) ([^"]+)" (\d+) (\d+) "([^"]+)" "([^"]+)"};

my %file;

while (<>) {
	chomp;
	/$log_pattern/;
	
	if ( $4 and $7 ) {
		$file{$4} += $7;
	}
}


print "traffic\tfile\n";
print "$file{$_}\t$_\n" foreach reverse sort { $file{$a} <=> $file{$b} } keys %file;
